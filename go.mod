module git.autistici.org/ai/webtools

go 1.14

require (
	github.com/blevesearch/bleve/v2 v2.3.4
	github.com/piranha/gostatic v0.0.0-20220509100602-78396eecdd4e
	github.com/russross/blackfriday/v2 v2.1.0
	golang.org/x/text v0.3.7
)
