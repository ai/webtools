// Convert old makefaq.py files to Markdown.
//
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
)

type Question struct {
	Title  string
	Answer string
}

type Section struct {
	Title     string
	Questions []*Question
}

type FAQ struct {
	Sections []*Section
}

const (
	stateUnknown = iota
	stateSection
	stateQuestion
	stateAnswer
)

type oldQuestion struct {
	section, question, answer string
}

// Reads FAQs in the makefaq.py format (more or less - actually, less:
// every <c> <q> or <a> directive has to be on a line by itself).
func loadDAT(r io.Reader) ([]*oldQuestion, error) {
	state := stateUnknown
	var parsed []*oldQuestion
	var curQuestion *oldQuestion
	var curStr string

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()
		newState := state
		switch line {
		case "<c>":
			newState = stateSection
		case "<q>":
			newState = stateQuestion
		case "<a>":
			newState = stateAnswer
		default:
			if curStr != "" {
				curStr += "\n"
			}
			curStr += line
		}
		if newState != state {
			switch state {
			case stateSection:
				if curQuestion != nil {
					parsed = append(parsed, curQuestion)
				}
				curQuestion = &oldQuestion{section: curStr}
			case stateQuestion:
				curQuestion.question = curStr
			case stateAnswer:
				curQuestion.answer = curStr
			}
			state = newState
			curStr = ""
		}
	}
	if curStr != "" {
		curQuestion.answer = curStr
		parsed = append(parsed, curQuestion)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return parsed, nil
}

func toFAQ(parsed []*oldQuestion) *FAQ {
	var sections []string
	tmp := make(map[string]*Section)
	for _, q := range parsed {
		sec, ok := tmp[q.section]
		if !ok {
			sections = append(sections, q.section)
			sec = &Section{Title: q.section}
			tmp[q.section] = sec
		}
		sec.Questions = append(sec.Questions, &Question{
			Title:  q.question,
			Answer: toMarkdown(q.answer),
		})
	}

	faq := new(FAQ)
	for _, s := range sections {
		faq.Sections = append(faq.Sections, tmp[s])
	}
	return faq
}

var (
	linkRx = regexp.MustCompile(`<a href="([^"]+)">([^<]*)</a>`)
	brRx   = regexp.MustCompile(`<br\s*/?>`)
)

func toMarkdown(s string) string {
	s = brRx.ReplaceAllLiteralString(s, "\n\n")
	s = linkRx.ReplaceAllString(s, "[${2}](${1})")
	return s
}

func main() {
	// Load old DAT format data.
	parsed, err := loadDAT(os.Stdin)
	if err != nil {
		log.Fatalf("ERROR: %v", err)
	}

	// Parse as FAQ.
	faq := toFAQ(parsed)

	// Render markdown.
	fmt.Printf("# FAQ\n\n")
	for _, section := range faq.Sections {
		fmt.Printf("## %s\n\n", section.Title)
		slug := strings.ToLower(strings.Replace(section.Title, " ", "_", -1))
		for i, q := range section.Questions {
			fmt.Printf("### <a name=\"%s_%d\"></a> ", slug, i+1)
			fmt.Printf("%s\n\n%s\n\n", q.Title, q.Answer)
		}
	}
}
