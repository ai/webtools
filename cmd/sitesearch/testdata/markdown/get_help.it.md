title: Niente panico!
----

Niente panico!
==============

Come trovare aiuto su A/I
-------------------------

In questa pagina ti spieghiamo cosa puoi fare quando credi di aver bisogno del nostro aiuto. Abbiamo realizzato alcuni strumenti per cercare
di ridurre il traffico di email. Prima di scriverci, quindi, **ti invitiamo caldamente a cercare qui la soluzione al tuo problema**.

- [Le domande più frequenti](/faq "FAQ") (con relative risposte)
- [Manuali](/docs/) - I manuali dei nostri servizi.
- [Helpdesk](http://helpdesk.autistici.org/) - qui puoi lasciare una comunicazione specifica riguardo al tuo problema, ti sarà risposto appena possibile (attenzione a lasciare un indirizzo email dove possiamo risponderti!)

In genere è possibile trovare qualcuno di noi anche sul canale IRC **\#ai** sui nostri server
[irc.autistici.org](/docs/irc/). Avendo tempo, qualcuno potrà darvi consigli e informazioni o risolvere
piccoli problemi tecnici.

<a name="gpgkey"></a>

Se siete proprio disperati e proprio non sapete cosa fare, [scriveteci](mailto:info@autistici.org), possibilmente
usando la nostra chiave GPG (disponibile su molti keyserver)

    pub   4096R/D98DA9CE 2012-12-13 [expires: 2017-12-12]
    uid                  Autistici / Inventati Staff (www.autistici.org / www.inventati.org) <info@autistici.org>
    sub   4096R/8FFE61D6 2012-12-13 [expires: 2017-12-12]

    Key fingerprint = E30D 5650 109E 5353 2104  B879 DA73 3D59 D98D A9CE

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Version: GnuPG v1.4.12 (GNU/Linux)

    mQINBFDKOjoBEADaG4KCnWx11VR8IdIE75SWt0ba/daMV3uZaVyGBGM8i+ouMqf3
    A7q2/ENOkPQuqrvoVZxHqs8PQHnWkzAE4/tv3ubdG/d1r05NDAJGfOf3W8S4h8iN
    wdQgjEPe1xLBYI5qMMP3hWDm0b7M+BMc27Zc+Owt6c4bCbxDfe3wr46M/mz15EzJ
    YNWZO5sHHuGtiTzsyQAguOpy/nwXhg3NeDHy/sON4INTYgJabbNeJs71fsXdNPq6
    k9FQrC23r/aTJHHkAdzf4TOrCfHj3RVjfesYzNYcRZKUcbXxlLAIz2sy6XGKXEMr
    7mQmOrhn1VJ/FK6iQP4CdaVzGHBzm3OnkooWFgl6ky/TdcN11WZvK7UhuSrLjzMO
    TFQwkrv7HnsHp5YrQujFUXehDyWwL+v6mCvyq4KTxjb2+phUthj59shqNN+CGbR7
    UTQ4330lpp7ioiGnTCSdABrLNh64gB6XQc19m+PLRHqTG06rIvhwSj2UB8BnncTg
    Ps1XQ23xSJwLCRE0xlCSFJIsy6g9xFi6yL+CCWhGkz4zCu1MaGkc08Bk92LI+dnz
    ZG/ZaChaitNzDVLZugJ9Wk9ql7qORx1OiI4+S1NQfdI08ecFfslt0kYQWMCWymXs
    +Lz0D5ZhBGdyXrCH69FpBRIFNzY14NaAsIe+sE+4qzVVw3D4dblFGrj+fQARAQAB
    tFhBdXRpc3RpY2kgLyBJbnZlbnRhdGkgU3RhZmYgKHd3dy5hdXRpc3RpY2kub3Jn
    IC8gd3d3LmludmVudGF0aS5vcmcpIDxpbmZvQGF1dGlzdGljaS5vcmc+iQI/BBMB
    AgApBQJQyjo6AhsDBQkJZgGABwsJCAcDAgEGFQgCCQoLBBYCAwECHgECF4AACgkQ
    2nM9WdmNqc6Z8BAAuGCxNVUwSsZL3+XyWEjhpF9hYKuMlbRaCCqni81TiMoho+0p
    q7jpPk+/5RvY9vptfD1FiKn8QJPBteByz9nhbfPA8g+0FzoHYmrts057OLwBugj8
    bL1K2YJ98CTli4/KfqAKgGeR8QyQOggqqnwrrxuswYthYxXDhVgMA7G0f+oKNGx6
    xvwXnCvnJ6oJ2edCdQfGXAC7JcFWKJcnIbuN4HSis85zKY0X4LWbx1saI6Y8EBHi
    QGziMY4ZH5sMr9Td4tXzIfhsTnBJuS8l/QoInb+ms8XO6RFt4e1Fk6fmlbZqV5i7
    AhHZk62BR1hekSRuzvk3cnLx2EfQhH1curXLRZTCzgy8bfgBLfI54u97AcvG9WNr
    c6apaHLiXF+RdeohUDGQ4egVRigVrJy2mED42QZ+Pt7WZ6LK2VqluyuyxiGQjseI
    TGZlqH9t/pjGQsBIVj3HWGbDbICAm3e0qBpbe36zAyQHO5GLlrlS09cdLa86Vbn7
    NlQTTYQMOvvA1XBXsszZtyYCIUgCVO4wkDpgIe7IwMvNS53Igf0JN16XrQZZd+VK
    10d7KWeoIch6GjOgmkDHaGk6lZ6Zsf4ORt8Rse2YtYVlOCpJxL7c+J4F+ICG/v0v
    1i1w3dby5l7juD1tCC8KWtT9oVEu27pqyboki0zfWxZJV+lwd+CXK4e6M6uIRgQQ
    EQIABgUCUMo7IgAKCRCeWP6tRJyGi2adAJ4yhoctjILFyEHtT9VFWdPprepymgCg
    m+B2vDQAf0MGJh9EdAG5HEdHkWC5Ag0EUMo6OgEQAK7r8oKZ6ZDWBHBe9VDcVsvd
    eRqZ+k056sOq+vRx1UR85vguPknJfSBnTL2F/PMp0BTtej6qbqeFsW+z63uLUgvE
    tjtohUmJRXaS/xk/qqa/POBjODGy4WjK/hDRMIRaOs0tpZD/SWY79pCo9KChghsN
    KAWZ3cUaTn/Mevus4fgaSZL++z9nq1B2/RsdP1AYZ5iqk1CJmtCA+qqaVFXnX7SZ
    LlodRUMSd4N3e3gExhK+s9ZJ54f9ITF+fNdHQwwpdUTC18pFYMKiwSCvJk3TYs/d
    1yRqyTQuYCCJYP7EN0E25L0iy8gvHVK+6J04+bzqsQ6xZ1IOJnbF3dtiCFVbkJFr
    L+nPG5sE0NDpMm08PhtM6lmm6g7PhTgOpvrGAmWUJXAvcG0J1eqxAabtJBhRE3lN
    edWyQlqkoJMXr3rxUB7TlqBJdi1tiWwRXuBr4Wn79R0Bo8pgFlcQ+tnMMvGKFyZb
    ljfDnP5CqlbQRAQSl6XpB+3Cjc+wZWz0gDdIYr8Dz1sjqR6VzllKU2lCZZ78W+9m
    52uhJaquM75gx67JC16mnNy4wmmbtOnC+1Q+WRAkPsipde8IaFqgMdinelRoBu+Y
    +GpcKJfVuxwomJJzgjHx3KoWKfWilp2juDE5Cl7aWsxYET4UwBGQbaiqfrinwCk1
    df4II3cm2mnrj3ehyov5ABEBAAGJAiUEGAECAA8FAlDKOjoCGwwFCQlmAYAACgkQ
    2nM9WdmNqc6ERQ/8CZxHPqyeQ2LCT6VTLAH9L4bDTJd8evmeKpI4gJDSlMbIdZCR
    EuHLn8JSGTpeINqc9rW3YY9E8evivJTQvCUsW7fMlRfX7kRJADkioskSjVNTEo46
    NLLp1Pdwa1XAHQQmKYI5cpt04PSSmdZhA2570UDmF0E76GmgJwNAnN/VvN4JgbfI
    YMQ2CVKzl+49wXPhPdO0JwKI+zocHytG1JwYvGyMMXyvbqylJTEUZ4BHQXdOGEfr
    osXLzdpD0kiZLDsTRMUbpayjpQD7d+PnBJ2IuslqfGGQIUQjCsJDr7FshNtIKOZc
    r9X9j4CbuI3mpVm0ZlsZg1a5BNQ5gkvjMJw4n5LHcJg/vdXaxZekk41nhmOMm3iQ
    uD76Fb+zN9UKlOpucPQ8wZmmTgF1sCpRkO99nOw7OP+laiAdFnj+Jqseg1rbNlsR
    TQGKasqGgrLHHRzy/bZJLnRO5Uaetl62sKbVnuz10wwRUQF1l6OXbDBiEA58k30v
    2bKRa7DWG/KaQQU2IV2bvbO8UETU5e1cv7fkEfSIwR4hR40yabnho8p6DUGUrF48
    3A0uojJvb61ZbsJHGYP4yORPTup4yP1A8gTyprh9t2C9wNDeQkE7oTSu69RT1HID
    urW5hlg8jEDmieJ5iOqKNT17ZcG04/BjCjg1rWclZ1Oka6qrDR4BoG2zEsM=
    =aBHh
    -----END PGP PUBLIC KEY BLOCK-----
