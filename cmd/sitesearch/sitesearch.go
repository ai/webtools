// sitesearch is a (quite simple) static site indexer.
//
// It is meant to work with a multi-language Markdown website, as it
// makes a lot of assumptions about the desired layout and it is
// basically only going to work well for the autistici.org website.
//
// One of the fundamental assumptions is that all pages are named
// something like "<name>.<language>.md" (so that it is possible to
// infer their language just by their filename).
//
// Indexing works by reading the website structure from a JSON-encoded
// file called the "site map" and the program expects to find it as
// "public/all_pages.json" below the site directory.
//
package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/blevesearch/bleve/v2"
	_ "github.com/blevesearch/bleve/v2/analysis/analyzer/keyword"
	_ "github.com/blevesearch/bleve/v2/analysis/analyzer/simple"
	_ "github.com/blevesearch/bleve/v2/analysis/lang/de"
	_ "github.com/blevesearch/bleve/v2/analysis/lang/en"
	_ "github.com/blevesearch/bleve/v2/analysis/lang/fr"
	_ "github.com/blevesearch/bleve/v2/analysis/lang/it"
	_ "github.com/blevesearch/bleve/v2/analysis/lang/pt"
	bq "github.com/blevesearch/bleve/v2/search/query"
	gostatic "github.com/piranha/gostatic/lib"
	bf "github.com/russross/blackfriday/v2"
	"golang.org/x/text/language"
)

var (
	doIndex     = flag.Bool("update", false, "generate the index")
	doSearch    = flag.Bool("search", false, "query the index")
	debug       = flag.Bool("debug", false, "enable debug logging")
	siteDir     = flag.String("dir", ".", "website root dir")
	indexDir    = flag.String("index", "index", "index dir")
	curLanguage = flag.String("lang", "en", "current language")
	httpAddr    = flag.String("http", "127.0.0.1:3301", "address to run the http server on")
	templateDir = flag.String("templates", "templates", "template directory")
)

// Document type for Bleve. Page metadata is read from the
// JSON-encoded site map (all_pages.json) directly into this object,
// the remaining fields are then filled in later.
//
// The list of supported languages is, unfortunately, hardcoded.
//
type pageMeta struct {
	// ID is the unique id, in our case this is the same as the URL.
	ID string `json:"id"`

	// Public URL of the page (pre content negotiation).
	URL string `json:"url"`

	// One different 'body' field per supported language. Note
	// that currently (12/2015) bleve still doesn't work properly
	// with nested index mappings (which would have removed some
	// of the ugliness of this implementation by letting us use,
	// say, a map). See
	// https://github.com/blevesearch/bleve/issues/228.
	BodyEN string `json:"body_en"`
	BodyIT string `json:"body_it"`
	BodyFR string `json:"body_fr"`
	BodyPT string `json:"body_pt"`
	BodyES string `json:"body_es"`
	BodyDE string `json:"body_de"`

	// Page title. Same as Body.
	TitleEN string `json:"title_en"`
	TitleIT string `json:"title_it"`
	TitleFR string `json:"title_fr"`
	TitlePT string `json:"title_pt"`
	TitleES string `json:"title_es"`
	TitleDE string `json:"title_de"`
}

func (r pageMeta) Type() string {
	return "page"
}

func (r *pageMeta) addTranslation(lang, title, body string) {
	switch lang {
	case "it":
		r.TitleIT = title
		r.BodyIT = body
	case "en":
		r.TitleEN = title
		r.BodyEN = body
	case "fr":
		r.TitleFR = title
		r.BodyFR = body
	case "pt":
		r.TitlePT = title
		r.BodyPT = body
	case "es":
		r.TitleES = title
		r.BodyES = body
	case "de":
		r.TitleDE = title
		r.BodyDE = body
	}
}

// Not all languages are supported by bleve. Use the 'simple' analyzer
// for them instead of the language-specific one.
func analyzerForLanguage(lang language.Tag) string {
	switch lang {
	case language.English, language.Italian, language.French, language.Portuguese, language.German:
		return lang.String()
	default:
		return "simple"
	}
}

// stripHTML returns s without HTML tags. It is fairly
// naive but works for most valid HTML inputs.
func stripHTML(s []byte) []byte {
	var buf bytes.Buffer
	var inTag, inQuotes bool
	var tagStart int
	for i, ch := range s {
		if inTag {
			if ch == '>' && !inQuotes {
				inTag = false
			} else if ch == '<' && !inQuotes {
				// false start
				buf.Write(s[tagStart:i])
				tagStart = i
			} else if ch == '"' {
				inQuotes = !inQuotes
			}
			continue
		}
		if ch == '<' {
			inTag = true
			tagStart = i
			continue
		}
		buf.WriteByte(ch)
	}
	if inTag {
		// false start
		buf.Write(s[tagStart:])
	}
	return buf.Bytes()
}

var headerSeparatorRx = regexp.MustCompile(`(?m:^----\r?\n)`)

// Read a Markdown file (with an optional metadata header) and render
// it as plain text.
func pageText(data []byte) string {
	// Remove the gostatic metadata header, if present.
	ds := string(data)
	parts := headerSeparatorRx.Split(ds, 2)
	if len(parts) == 2 {
		ds = parts[1]
	}

	// set up the HTML renderer
	renderer := bf.NewHTMLRenderer(bf.HTMLRendererParameters{
		Flags: bf.Smartypants | bf.SmartypantsFractions,
	})

	// Convert the markdown data to HTML first, then to text
	// (blackfriday does not have a plain text output module, we
	// can only pick between HTML and Latex...)
	html := bf.Run(
		[]byte(ds),
		bf.WithRenderer(renderer),
		bf.WithExtensions(bf.CommonExtensions),
	)
	return string(stripHTML(html))
}

// Page as represented in the JSON-encoded site map.
type siteMapPage struct {
	URL   string `json:"url"`
	Lang  string `json:"lang"`
	Title string `json:"title"`
}

// Load the JSON-encoded site map from all_pages.json.
func loadSiteMap(path string) (map[string]*siteMapPage, error) {
	var siteMap map[string]*siteMapPage
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close() // nolint
	if err := json.NewDecoder(f).Decode(&siteMap); err != nil {
		return nil, err
	}
	return siteMap, nil
}

// Regular expression that "inverts" the effects of content
// negotiation, giving us a public URL from the specific URL of a
// page. Removes the .html extension, any language-specific extension,
// and the 'index' name from index pages.
var langExtensionRx = regexp.MustCompile(`(index)?\.[a-z]{2,3}\.html$`)

// Load the pages from the site map.
func loadSite(dir string) ([]*pageMeta, error) {
	siteMap, err := loadSiteMap(filepath.Join(dir, "public/all_pages.json"))
	if err != nil {
		return nil, err
	}

	// Process the page objects and add the missing fields.
	tmp := make(map[string]*pageMeta)
	for path, page := range siteMap {
		// Skip incomplete entries.
		if page.Title == "" || page.Lang == "" || page.URL == "" {
			continue
		}

		// Remove extensions to get the bare (pre-content-negotiation) URL.
		metaURL := langExtensionRx.ReplaceAllString(page.URL, "")

		meta, ok := tmp[metaURL]
		if !ok {
			meta = &pageMeta{
				ID:  metaURL,
				URL: metaURL,
			}
			tmp[metaURL] = meta
		}

		// Load the page data and convert it to text.
		data, err := ioutil.ReadFile(filepath.Join(dir, path))
		if err != nil {
			return nil, err
		}
		meta.addTranslation(page.Lang, page.Title, pageText(data))
	}

	var out []*pageMeta
	for _, p := range tmp {
		out = append(out, p)
	}
	return out, nil
}

var knownLanguages = []language.Tag{
	language.English,
	language.Italian,
	language.French,
	language.Portuguese,
	language.Spanish,
	language.German,
}

var requestFields = []string{"url"}

func init() {
	for _, l := range knownLanguages {
		requestFields = append(requestFields, "title_"+l.String())
	}
}

func openIndex(dir string, readOnly bool) (bleve.Index, error) {
	var index bleve.Index
	// Create the index if it does not exist.
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		// We are not going to create a new db if we've been
		// asked to open read-only.
		if readOnly {
			return nil, err
		}

		// Create all language analyzers we need.
		doc := bleve.NewDocumentStaticMapping()
		for _, lang := range knownLanguages {
			textFieldMapping := bleve.NewTextFieldMapping()
			textFieldMapping.Analyzer = analyzerForLanguage(lang)
			textFieldMapping.IncludeInAll = false
			doc.AddFieldMappingsAt("body_"+lang.String(), textFieldMapping)
			doc.AddFieldMappingsAt("title_"+lang.String(), textFieldMapping)
		}

		auxFieldMapping := bleve.NewTextFieldMapping()
		auxFieldMapping.Index = false
		auxFieldMapping.Store = true
		auxFieldMapping.IncludeInAll = false
		auxFieldMapping.IncludeTermVectors = false
		for _, f := range []string{"id", "url"} {
			doc.AddFieldMappingsAt(f, auxFieldMapping)
		}

		indexMap := bleve.NewIndexMapping()
		indexMap.AddDocumentMapping("page", doc)

		if index, err = bleve.New(dir, indexMap); err != nil {
			return nil, fmt.Errorf("error creating index: %w", err)
		}
	} else {
		if index, err = bleve.OpenUsing(dir, map[string]interface{}{
			"read_only": readOnly,
		}); err != nil {
			return nil, fmt.Errorf("error opening index: %w", err)
		}
	}
	return index, nil
}

var (
	// How much to boost results in the user's language, compared to other languages.
	ownLanguageBoostFactor = 2.0

	// How much to boost titles.
	titleBoostFactor = 7.0
)

// Search the index and return the results.
func search(index bleve.Index, args []string, primaryLanguage string, limit, offset int) *bleve.SearchResult {
	// Build the query as a series of boolean ORs across all
	// possible language fields, for both body and title.
	var langq []bq.Query
	for _, pfx := range []string{"body_", "title_"} {
		for _, lang := range knownLanguages {
			lq := bleve.NewBooleanQuery()
			for _, term := range args {
				q := bleve.NewMatchQuery(term)
				q.SetField("body_" + lang.String())
				lq.AddMust(q)
			}
			boost := 1.0
			if lang.String() == primaryLanguage {
				boost *= ownLanguageBoostFactor
			}
			if pfx == "title_" {
				boost *= titleBoostFactor
			}
			if boost > 1 {
				lq.SetBoost(boost)
			}
			langq = append(langq, lq)
		}
	}
	query := bleve.NewDisjunctionQuery(langq...)

	if *debug {
		qdebug, _ := bq.DumpQuery(index.Mapping(), query)
		log.Printf("%s", qdebug)
	}

	// Request more results than the limit so that language deduplication
	// later on is more effective, for instance in the case where the page
	// in the preferred language scores lower than the same in another
	// language. Change the factor based on performance and the average
	// number of translations per page.
	request := bleve.NewSearchRequestOptions(query, limit, offset, true)
	request.Fields = requestFields
	result, err := index.Search(request)
	if err != nil {
		log.Printf("error during query (q=%s, lang=%s): %v", strings.Join(args, " "), primaryLanguage, err)
		return nil
	}
	if *debug {
		log.Printf("%v", result)
	}
	return result
}

// The search results.
type searchResult struct {
	Hits []searchHit `json:"hits"`
}

type searchHit struct {
	URL   string  `json:"url"`
	Title string  `json:"title"`
	Score float64 `json:"score"`
}

var fallbackLanguages = []string{"en", "it"}

// The result hits should have a single 'title' field, synthesized from the
// language-specific ones based on the user's preferred language.
func mangleResult(result *bleve.SearchResult, primaryLanguage string) *searchResult {
	var out searchResult
	out.Hits = make([]searchHit, 0, len(result.Hits))
	for _, h := range result.Hits {
		var title string
		if s, ok := h.Fields["title_"+primaryLanguage]; ok && s != "" {
			title = s.(string)
		} else {
			for _, l := range fallbackLanguages {
				if s, ok := h.Fields["title_"+l]; ok && s != "" {
					title = s.(string)
					break
				}
			}
		}
		if title == "" {
			continue
		}
		out.Hits = append(out.Hits, searchHit{
			URL:   h.Fields["url"].(string),
			Title: title,
			Score: h.Score,
		})
	}
	return &out
}

type server struct {
	index bleve.Index
	tpl   *template.Template
}

func newServer(index bleve.Index, templatedir string) (*server, error) {
	// Load the gostatic template functions.
	tpl := template.New("").Funcs(template.FuncMap(gostatic.TemplateFuncMap))
	if _, err := tpl.ParseGlob(filepath.Join(templatedir, "*.tmpl")); err != nil {
		return nil, err
	}
	return &server{
		index: index,
		tpl:   tpl,
	}, nil
}

func intFormValue(req *http.Request, name string, defaultValue int) int {
	if s := req.FormValue(name); s != "" {
		if value, err := strconv.Atoi(s); err == nil && value > 0 {
			return value
		}
	}
	return defaultValue
}

// Parse a query string (possibly empty) into search terms. We can't use the
// fancy query string parsers from Bleve because we need to assemble the final
// Query ourselves, so we just naively parse the string as whitespace-separated
// tokens.
func parseQuery(q string) []string {
	var terms []string
	for _, s := range strings.Split(q, " ") {
		if s != "" {
			terms = append(terms, s)
		}
	}
	return terms
}

func (s server) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	qstr := strings.TrimSpace(req.FormValue("q"))
	q := parseQuery(qstr)
	lang := languageFromRequest(req)
	limit := intFormValue(req, "n", 20)
	offset := intFormValue(req, "o", 0)

	outfmt := "html"
	if fstr := req.FormValue("fmt"); fstr != "" {
		outfmt = fstr
	}

	// Only run a query if the query string is actually set.
	var result *searchResult
	if len(q) > 0 {
		result = mangleResult(search(s.index, q, lang, limit, offset), lang)
		// Set cache-busting headers for query results page.
		w.Header().Set("Cache-Control", "no-store")
		w.Header().Set("Expires", "-1")
	}

	var err error
	switch outfmt {
	case "json":
		ctx := struct {
			Query  string        `json:"query"`
			Result *searchResult `json:"result"`
		}{
			Query:  qstr,
			Result: result,
		}
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(&ctx)

	default:
		// Enough of the gostatic page context to render our template.
		// We fake a Path with a language suffix just so that the
		// Javascript code in the page can figure out the current
		// language.
		ctx := struct {
			Query  string
			Url    string // nolint
			Path   string
			Title  string
			Result *searchResult
		}{
			Query:  qstr,
			Url:    "/search",
			Path:   fmt.Sprintf("/search.%s.html", lang),
			Title:  "Search",
			Result: result,
		}

		// Evaluate the 'search_results' template.
		var buf bytes.Buffer
		if err = s.tpl.ExecuteTemplate(&buf, "search_results", &ctx); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Printf("error rendering template: %v", err)
			return
		}
		_, err = buf.WriteTo(w)
	}

	if err != nil {
		log.Printf("error writing response: %v", err)
	}
}

// Return the user language from the HTTP request. This can come
// either from the site-wide language preference cookie
// (site_language), or from the Accept-Language HTTP header.
func languageFromRequest(req *http.Request) string {
	if cookie, err := req.Cookie("site_language"); err == nil {
		return cookie.Value
	}

	if hdr := req.Header.Get("Accept-Language"); hdr != "" {
		m := language.NewMatcher(knownLanguages)
		tags, _, _ := language.ParseAcceptLanguage(hdr)
		t, _, _ := m.Match(tags...)
		return t.String()
	}

	return "en"
}

func main() {
	flag.Parse()

	// Only open the database in read-write mode when using --index.
	idx, err := openIndex(*indexDir, !*doIndex)
	if err != nil {
		log.Fatal(err)
	}
	defer idx.Close() // nolint

	bleve.SetLog(log.New(os.Stderr, "bleve: ", 0))

	if *doSearch {
		result := search(idx, flag.Args(), *curLanguage, 20, 0)
		fmt.Printf("%v\n", result)
	} else if *doIndex {
		pdata, err := loadSite(*siteDir)
		if err != nil {
			log.Fatal(err)
		}

		b := idx.NewBatch()
		for i, pd := range pdata {
			if err := b.Index(pd.ID, pd); err != nil {
				log.Printf("error indexing %s: %v", pd.ID, err)
				continue
			}
			if i%7 == 0 {
				fmt.Printf("%8d / %d\r", i, len(pdata))
				os.Stdout.Sync() // nolint
			}
		}
		fmt.Printf("%8d / %d\r", len(pdata), len(pdata))
		os.Stdout.Sync() // nolint
		if err := idx.Batch(b); err != nil {
			log.Fatalf("error inserting data: %v", err)
		}
		fmt.Printf("indexed %d pages             \n", len(pdata))
	} else if *httpAddr != "" {
		srv, err := newServer(idx, *templateDir)
		if err != nil {
			log.Fatal(err)
		}
		if err := http.ListenAndServe(*httpAddr, srv); err != nil {
			log.Fatal(err)
		}
	} else {
		log.Fatal("Nothing to do!")
	}
}
