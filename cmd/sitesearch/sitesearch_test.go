package main

import (
	"os"
	"testing"

	"github.com/blevesearch/bleve/v2"
)

func TestLoadSite(t *testing.T) {
	pages, err := loadSite("testdata")
	if err != nil {
		t.Fatal("loadSite():", err)
	}
	if len(pages) != 2 {
		t.Fatalf("expected 2 pages, got %d", len(pages))
	}
}

type testIndex struct {
	idx bleve.Index
}

func newTestIndex(t testing.TB, load bool) *testIndex {
	idx, err := openIndex(".index-tmp", false)
	if err != nil {
		t.Fatal("openIndex():", err)
	}

	if load {
		b := idx.NewBatch()
		pdata, _ := loadSite("testdata")
		for _, pd := range pdata {
			b.Index(pd.ID, pd) // nolint
		}
		if err := idx.Batch(b); err != nil {
			t.Fatal("Batch():", err)
		}
	}

	return &testIndex{idx: idx}
}

func (i *testIndex) Close() {
	i.idx.Close()
	os.RemoveAll(".index-tmp")
}

func TestIndex_Create(t *testing.T) {
	i := newTestIndex(t, true)
	i.Close()
}

func TestIndex_Search(t *testing.T) {
	i := newTestIndex(t, true)
	defer i.Close()

	result := search(i.idx, []string{"progetto"}, "it", 20, 0)
	if result == nil {
		t.Fatal("nil result")
	}
	if len(result.Hits) != 1 {
		t.Fatalf("expected 1 result, got: %q", result)
	}
	if u := result.Hits[0].Fields["url"].(string); u != "/about" {
		t.Fatalf("bad URL: expected /about, got %s", u)
	}
}

type testQuery struct {
	query string
	lang  string
}

// Test a bunch of queries that should all return a single result
// (whose URL is expectURL).
func testQueries(t testing.TB, idx bleve.Index, queries []testQuery, expectURL string) {
	for _, q := range queries {
		result := mangleResult(search(idx, []string{q.query}, q.lang, 20, 0), q.lang)
		if result == nil {
			t.Errorf("search(%s, %s): nil result", q.query, q.lang)
			continue
		}
		if len(result.Hits) != 1 {
			t.Errorf("search(%s, %s): expected 1 result, got %d: %v", q.query, q.lang, len(result.Hits), result)
			continue
		}
		if u := result.Hits[0].URL; u != expectURL {
			t.Errorf("search(%s, %s): bad URL: expected %s, got %s", q.query, q.lang, expectURL, u)
		}
		if s := result.Hits[0].Title; s == "" {
			t.Errorf("search(%s, %s): no 'title' field set", q.query, q.lang)
		}
	}
}

func TestIndex_Search_AllLanguages(t *testing.T) {
	i := newTestIndex(t, true)
	defer i.Close()

	// Make the same search for all languages, verify that it
	// returns the page even in case of search language mismatch
	// (token appears only in the Italian translation).
	var queries1 []testQuery
	for _, lang := range knownLanguages {
		queries1 = append(queries1, testQuery{query: "progetto", lang: lang.String()})
	}
	testQueries(t, i.idx, queries1, "/about")

	// Same as above, but for a page for which there are no
	// translations.
	var queries2 []testQuery
	for _, lang := range knownLanguages {
		queries2 = append(queries2, testQuery{query: "aiuto", lang: lang.String()})
	}
	testQueries(t, i.idx, queries2, "/get_help")

	// Now search for a specific term in every translation.
	queries3 := []testQuery{
		{"progetto", "it"},
		//{"privacitat", "ca"},
		{"basisdemokratischen", "de"},
		{"grassroot", "en"},
		{"politique", "fr"},
	}
	testQueries(t, i.idx, queries3, "/about")
}
