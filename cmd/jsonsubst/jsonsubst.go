// Run text/template substitution on a file with context read from
// another separate JSON-encoded file.

package main

import (
	"encoding/json"
	"flag"
	"text/template"
	"io"
	"io/ioutil"
	"log"
	"os"
)

var (
	inputFile = flag.String("input", "", "input file (defaults to stdin)")
	dataFile = flag.String("data", "", "input file (defaults to stdin)")
)

func expand(templateIn io.Reader, data interface{}, w io.Writer) error {
	tplStr, err := ioutil.ReadAll(templateIn)
	if err != nil {
		return err
	}
	tpl, err := template.New("template").Parse(string(tplStr))
	if err != nil {
		return err
	}
	return tpl.Execute(w, data)
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if *dataFile == "" {
		log.Fatal("Must specify --data")
	}
	dataReader, err := os.Open(*dataFile)
	if err != nil {
		log.Fatal(err)
	}
	defer dataReader.Close()

	var templateReader io.Reader
	if *inputFile != "" {
		f, err := os.Open(*inputFile)
		if err != nil {
			log.Fatalf("error opening input file: %v", err)
		}
		defer f.Close()
		templateReader = f
	} else {
		templateReader = os.Stdin
	}

	var data interface{}
	if err := json.NewDecoder(dataReader).Decode(&data); err != nil {
		log.Fatalf("error decoding JSON data: %v", err)
	}

	if err := expand(templateReader, data, os.Stdout); err != nil {
		log.Fatal(err)
	}
}
